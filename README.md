# DotNet generation code With Angular Schematics

This sample showing, how do you can use Angular Schematics for DotNet projects.

[Schematic repo.](https://gitlab.com/yaniii/schematic-dotnet)

Do you have any questions or problems, please contact me: 
- Telegram/Twitter: @yani4218
- email: yani4218@gmail.com

Step 1:

Setup nodeJS.

Step 2:

install Schematics CLI:
```bash
npm i -g @angular-devkit/schematics-cli
```

Step 3:
Open directory `dotnet-schematics` at the terminal and install packages:
```bash
npm i
```

Step 4:

Command: `schematics <path-to-collection.json>:<schematic-name or schematic-aliases> <schematic-options> --dry-run=false`, will run execute schematic.

Good luck, my friend!
From scout-ngx team with ❤️
