"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const welcom_1 = require("./utility/messages/welcom");
const result_1 = require("./utility/messages/result");
const add_files_1 = require("./utility/add-files");
// You don't have to export the function as default. You can also have more than one rule factory
// per file.
function dotNetController(_options) {
    return (_tree, _context) => {
        // Chain multiple rules into a single rule.
        return schematics_1.chain([
            () => welcom_1.addWelcomMessage(),
            () => add_files_1.addFilesToTree(_options),
            () => result_1.addResultMessage()
        ]);
    };
}
exports.dotNetController = dotNetController;
//# sourceMappingURL=index.js.map