"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const testing_1 = require("@angular-devkit/schematics/testing");
const path = require("path");
const collectionPath = path.join(__dirname, '../collection.json');
const runner = new testing_1.SchematicTestRunner('schematics', collectionPath);
let testTree;
describe('react-component', () => {
    beforeEach(() => {
        testTree = runner.runSchematic('custom-controller', {}, schematics_1.Tree.empty());
    });
    it('check add files', () => {
        expect(testTree.files).toEqual(['/Name.cs']);
    });
    it('check file content', () => {
        const fileContentBuffer = testTree.read('/Name.cs');
        const fileContent = fileContentBuffer ? fileContentBuffer.toString() : '';
        expect(fileContent.indexOf('Name') !== -1).toBe(true);
    });
});
//# sourceMappingURL=index_spec.js.map