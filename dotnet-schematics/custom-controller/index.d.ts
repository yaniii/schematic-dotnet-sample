import { Rule } from '@angular-devkit/schematics';
import { schemaOptions } from './schema';
export declare function dotNetController(_options: schemaOptions): Rule;
