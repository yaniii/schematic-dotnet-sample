"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addResultMessage = () => {
    return (_tree, _context) => {
        _context.logger.log('info', `Done 🔥  See you later ✌️`);
        return _tree;
    };
};
//# sourceMappingURL=result.js.map