"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addWelcomMessage = () => {
    return (_tree, _context) => {
        _context.logger.log('info', `Hi I'm angular schematic. I will do everything myself, and you could drink ☕️  with 🍩`);
        _context.logger.log('info', `From scout-ngx team with ❤️`);
        return _tree;
    };
};
//# sourceMappingURL=welcom.js.map