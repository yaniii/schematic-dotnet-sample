"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const core_1 = require("@angular-devkit/core");
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const stringUtils = { dasherize: strings_1.dasherize, classify: strings_1.classify };
exports.addFilesToTree = (_options) => {
    _options.path = _options.path ? core_1.normalize(_options.path) : _options.path;
    const templateSource = schematics_1.apply(schematics_1.url('./files'), [
        schematics_1.applyTemplates(Object.assign(Object.assign({}, stringUtils), _options)),
        schematics_1.move(core_1.normalize(_options.path))
    ]);
    return schematics_1.branchAndMerge(schematics_1.chain([schematics_1.mergeWith(templateSource)]));
};
//# sourceMappingURL=add-files.js.map