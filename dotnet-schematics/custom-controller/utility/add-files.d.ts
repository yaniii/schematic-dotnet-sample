import { Rule } from '@angular-devkit/schematics';
import { schemaOptions } from '../schema';
export declare const addFilesToTree: (_options: schemaOptions) => Rule;
